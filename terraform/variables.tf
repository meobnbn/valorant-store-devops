variable "region" {
    default = "eu-west-1"
    description = "valstore-infra region"
}

variable "security_group_id" {
    default = "sg-0021bc9237a5ffd7f"
    description = "valstore-infra security group id"
}

variable "cluster_name" {
    default = "valstore-cluster"
    description = "valstore-infra cluster name"
}

variable "image_tag" {
    default = "latest"
    description = "valstore-infra image version"
}

variable "container_repository" {
    default = ""
    description = "valstore-infra container repository"
}

variable "subnet_id" {
  default = "subnet-00b55fd0e33c185dd"
  description = "valstore-infra vpc id"
}