resource "aws_ecs_task_definition" "valstore-td" {
  family = "valstore-service"
  container_definitions    = jsonencode(
    [
      {
          "name": "valstore-container",
          "cpu": 256,
          "memory": 512,
          "image": "${var.container_repository}:${var.image_tag}",
          "essential": true,
          "portMappings": [
              {
                  "hostPort": 5000,
                  "containerPort": 5000,
                  "protocol": "tcp"
              }
          ],
          "requiresCompatibilities": [
              "FARGATE"
          ]
      }
  ])
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn
}

resource "aws_ecs_service" "valstore-service" {
  name            = "valstore-service"
  cluster         = data.aws_ecs_cluster.valstore-cluster.id
  task_definition = aws_ecs_task_definition.valstore-td.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets = [data.aws_subnet.valstore-sub.id]
    assign_public_ip = true
    security_groups = [data.aws_security_group.valstore-sg.id]
  }
}

data "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
}

data "aws_ecs_cluster" "valstore-cluster" {
  cluster_name = var.cluster_name
}

data "aws_security_group" "valstore-sg" {
  id = var.security_group_id
}

data "aws_subnet" "valstore-sub" {
  id = var.subnet_id
}